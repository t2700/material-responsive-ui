import React from "react";
import { Box, Breadcrumbs, Link, Typography } from "@mui/material";
import NavigateNextIcon from "@mui/icons-material/NavigateNext";

function MuiBreadcrumbs() {
  return (
    <Box m={2}>
      <Breadcrumbs
        aria-aria-label="breadcrumb"
        separator={<NavigateNextIcon fontSize="small"/>}
        maxItems={2}
        // itemsAfterCollapse={2} // last second item
        itemsBeforeCollapse={2} // for starting two item
      >
        <Link underline="hover" href="#">
          Home
        </Link>
        <Link underline="hover" href="#">
          Catalog
        </Link>
        <Link underline="hover" href="#">
          Accessories
        </Link>
        <Typography color="text.primary">Shoes</Typography>
      </Breadcrumbs>
    </Box>
  );
}

export default MuiBreadcrumbs;
