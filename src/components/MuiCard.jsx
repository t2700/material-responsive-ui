import React from 'react';
import {Box,Card,CardContent, Typography,CardActions,Button,CardMedia} from '@mui/material';

function MuiCard() {
  return (
    <Box width='300px'>
        <Card>
            <CardMedia 
            component='img' 
            height='140'
            image='https://cdn.pixabay.com/photo/2015/04/19/08/33/flower-729512__340.jpg'
            alt='unsplash image'
             />
            <CardContent>
                <Typography gutterBottom variant='h5' component='div'>
                    React
                </Typography>
                <Typography variant='body2' color='text.secondary'>
                    Lorem ipsum dolor sit amet consectetur, adipisicing elit. Nemo id velit maiores? Et sunt distinctio explicabo laudantium odit, a ut eaque rerum aliquid molestias tempora, quae ex libero, totam illo.
                </Typography>
            </CardContent>
            <CardActions>
                <Button size='small'>Share</Button>
                <Button size='small'>Learn more</Button>
            </CardActions>
        </Card>
    </Box>
  )
}

export default MuiCard