import React from 'react';
import {Typography} from '@mui/material'
 
function MuiTypography() {
  return (
    <div>
        <Typography variant="h1">h1 Heading</Typography>
        <Typography variant="h2">h2 Heading</Typography>
        <Typography variant="h3">h3 Heading</Typography>
        <Typography variant="h4" component='h1' gutterBottom>h4 Heading</Typography>
        <Typography variant="h5">h5 Heading</Typography>
        <Typography variant="h6">h6 Heading</Typography>
        <Typography variant="subtitle1">Subtitle1</Typography>
        <Typography variant="subtitle2">Subtitle2</Typography>
        <Typography variant="">Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptate, enim recusandae quaerat explicabo ducimus optio ratione adipisci odit dolores beatae totam. Eum, libero. Facere, ex delectus distinctio earum pariatur deserunt!</Typography>
        <Typography variant="body2">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Non sunt similique, in assumenda quia sed fuga impedit, quae quaerat nostrum odit nulla adipisci exercitationem ratione debitis veritatis aut officia necessitatibus.</Typography>

    </div>
  )
}

export default MuiTypography