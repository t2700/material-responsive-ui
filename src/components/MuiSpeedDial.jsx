import React from 'react'
import {SpeedDial,SpeedDialAction,SpeedDialIcon} from '@mui/material';
import CopyAllIcon from '@mui/icons-material/CopyAll';
import PrintIcon from '@mui/icons-material/Print';
import ShareIcon from '@mui/icons-material/Share';
import EditIcon from '@mui/icons-material/Edit';

function MuiSpeedDial() {
  return (
  <SpeedDial ariaLabel='Navigation speed dial'
  sx={{position:'absolute',bottom:16,right:16}}
  icon={<SpeedDialIcon openIcon={<EditIcon/>}/>}
  >
      <SpeedDialAction icon={<CopyAllIcon/>} tooltipTitle='copy' tooltipOpen/>
      <SpeedDialAction icon={<PrintIcon/>} tooltipTitle='print' tooltipOpen/>
      <SpeedDialAction icon={<ShareIcon/>} tooltipTitle='share' tooltipOpen/>
  </SpeedDial>
  )
}

export default MuiSpeedDial