import React from 'react'
import {TableContainer,Table,TableHead,TableBody,TableRow,TableCell,Paper,} from '@mui/material'
function MuiTable() {
  return (
   <TableContainer component={Paper} sx={{maxHeight:'300px'}}>
       <Table aria-label='simple table' stickyHeader>
        <TableHead>
            <TableRow>
                <TableCell>Id</TableCell>
                <TableCell>First Name</TableCell>
                <TableCell>Last Name</TableCell>
                <TableCell align='center'>Email</TableCell>
            </TableRow>
        </TableHead>
        <TableBody>
            {
                tableData.map(row=>(
                    <TableRow
                    key={row.id}
                    sx={{'&:last-child td, &:last-child th':{border:0}}}
                    >
                        <TableCell>{row.id}</TableCell>
                        <TableCell>{row.first_name}</TableCell>
                        <TableCell>{row.last_name}</TableCell>
                        <TableCell align='center'>{row.email}</TableCell>
                    </TableRow>
                ))
            }
        </TableBody>
       </Table>
   </TableContainer>
  )
}

export default MuiTable

const tableData=[{
    "id": 1,
    "first_name": "Ulysses",
    "last_name": "Veneur",
    "email": "uveneur0@usa.gov",
    "gender": "Male",
    "ip_address": "70.162.96.18"
  }, {
    "id": 2,
    "first_name": "Margy",
    "last_name": "O'Cosgra",
    "email": "mocosgra1@admin.ch",
    "gender": "Female",
    "ip_address": "82.44.112.45"
  }, {
    "id": 3,
    "first_name": "Saleem",
    "last_name": "Rikkard",
    "email": "srikkard2@amazon.co.uk",
    "gender": "Male",
    "ip_address": "93.153.252.251"
  }, {
    "id": 4,
    "first_name": "Mace",
    "last_name": "Saffle",
    "email": "msaffle3@house.gov",
    "gender": "Genderfluid",
    "ip_address": "102.0.126.158"
  }, {
    "id": 5,
    "first_name": "Carr",
    "last_name": "Beckhouse",
    "email": "cbeckhouse4@acquirethisname.com",
    "gender": "Male",
    "ip_address": "226.144.150.21"
  }, {
    "id": 6,
    "first_name": "Agnes",
    "last_name": "MacCall",
    "email": "amaccall5@wsj.com",
    "gender": "Female",
    "ip_address": "183.2.149.92"
  }, {
    "id": 7,
    "first_name": "Ilse",
    "last_name": "Gubbins",
    "email": "igubbins6@google.es",
    "gender": "Female",
    "ip_address": "86.16.138.63"
  }, {
    "id": 8,
    "first_name": "Jackelyn",
    "last_name": "Bachnic",
    "email": "jbachnic7@theatlantic.com",
    "gender": "Genderfluid",
    "ip_address": "31.194.96.174"
  }, {
    "id": 9,
    "first_name": "Fairfax",
    "last_name": "Seer",
    "email": "fseer8@tuttocitta.it",
    "gender": "Male",
    "ip_address": "100.52.127.98"
  }, {
    "id": 10,
    "first_name": "Nanon",
    "last_name": "Blabey",
    "email": "nblabey9@blogs.com",
    "gender": "Female",
    "ip_address": "141.212.228.126"
  }]
  
  