import React from "react";
import {
  AppBar,
  IconButton,
  Toolbar,
  Typography,
  Stack,
  Button,
  Menu,
  MenuItem
} from "@mui/material";
import {useState} from 'react';
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
import CatchingPokemonIcon from "@mui/icons-material/CatchingPokemon";
function MuiNavbar() {
  const [anchorE1,setAnchorE1] = useState(null)
  const  open = Boolean(anchorE1)
  const handleClick = (e)=>{
    setAnchorE1(e.target)
  }
  const handleClose = (e)=>{
    setAnchorE1(null)
  }
  return (
    <AppBar position="static">
      <Toolbar>
        <IconButton size="large" edge="start" color="inherit" aria-label="logo">
          <CatchingPokemonIcon />
        </IconButton>
        <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
          POKEMONAPP
        </Typography>
        <Stack direction="row" spacing={2}>
          <Button color="inherit">Features</Button>
          <Button color="inherit">Pricing</Button>
          <Button color="inherit">About</Button>
          <Button color="inherit" id='resource-button'
           onClick={handleClick} 
           aria-controls={open ? 'resources-menu': undefined}
           aria-haspopup='true'
           aria-expanded={open?'true':undefined}
           endIcon={<KeyboardArrowDownIcon/>}
           >Resources</Button>
          <Button color="inherit">Login</Button>
        </Stack>
        <Menu id='resources-menu'
         anchorEl={anchorE1}
          open={open}
          MenuListProps={{
            'aria-labelledby':'resources-button',
          }}
          onClose={handleClose}
           anchorOrigin={{
             vertical:'bottom',
             horizontal:'right',
           }}
           transformOrigin={{
             vertical:'top',
             horizontal:'right',
           }}
          >
            <MenuItem onClick={handleClose}>Blog</MenuItem>
            <MenuItem onClick={handleClose}>Podcast</MenuItem>
        </Menu>
      </Toolbar>
    </AppBar>
  );
}

export default MuiNavbar;
