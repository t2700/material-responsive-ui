import React from "react";
import { ImageListItemBar,Box,Stack, ImageList, ImageListItem } from "@mui/material";

function l() {
  const imageData = [
    {
      img: "https://cdn.pixabay.com/photo/2018/06/30/14/31/breakfast-3507706_960_720.jpg",
      title: "Breakfast",
    },
    {
      img: "https://cdn.pixabay.com/photo/2021/12/09/20/46/burger-6859072__340.jpg",
      title: "Burger",
    },
    {
      img: "https://cdn.pixabay.com/photo/2019/07/03/09/43/camera-4314039_960_720.jpg",
      title: "camera",
    },
    {
      img: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ0KU9Ojq-T-5ckvgqFY8iWQM0p4wLIqMXaXQ&usqp=CAU",
      title: "Cofee",
    },
    {
      img: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRrQFem8ZHJS440XEi7ACsbTR3rBTXtds2g2Q&usqp=CAU",
      title: "Flower",
    },
    {
      img: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTUkHkLF2-8Nj_lBHOOuFMa_b9XpUjj5v6w2Q&usqp=CAU",
      title: "Nature",
    },
    {
      img: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQv4qTlFZcK34lhjX4BwkO08RRNWjY1QyS7og&usqp=CAU",
      title: "Meals",
    },
    {
      img: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSyrCYAblBhipIEgZaxsNNGqCEu2LkFAQ36Rw&usqp=CAU",
      title: "Animals",
    },
  ];
  const imageData3 = [
    {
      img: "https://cdn.pixabay.com/photo/2018/06/30/14/31/breakfast-3507706_960_720.jpg",
      title: "Breakfast",
    },
    {
      img: "https://cdn.pixabay.com/photo/2021/12/09/20/46/burger-6859072__340.jpg",
      title: "Burger",
    },
    {
      img: "https://cdn.pixabay.com/photo/2019/07/03/09/43/camera-4314039_960_720.jpg",
      title: "camera",
    },
    {
      img: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ0KU9Ojq-T-5ckvgqFY8iWQM0p4wLIqMXaXQ&usqp=CAU",
      title: "Cofee",
    },
    {
      img: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRrQFem8ZHJS440XEi7ACsbTR3rBTXtds2g2Q&usqp=CAU",
      title: "Flower",
    },
    {
      img: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTUkHkLF2-8Nj_lBHOOuFMa_b9XpUjj5v6w2Q&usqp=CAU",
      title: "Nature",
    },
    {
      img: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQv4qTlFZcK34lhjX4BwkO08RRNWjY1QyS7og&usqp=CAU",
      title: "Meals",
    },
    {
      img: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSyrCYAblBhipIEgZaxsNNGqCEu2LkFAQ36Rw&usqp=CAU",
      title: "Animals",
    },
  ];
  const imageData2 = [
    {
      img: "https://cdn.pixabay.com/photo/2018/06/30/14/31/breakfast-3507706_960_720.jpg",
      title: "Breakfast",
    },
    {
      img: "https://cdn.pixabay.com/photo/2021/12/09/20/46/burger-6859072__340.jpg",
      title: "Burger",
    },
    {
      img: "https://cdn.pixabay.com/photo/2019/07/03/09/43/camera-4314039_960_720.jpg",
      title: "camera",
    },
    {
      img: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ0KU9Ojq-T-5ckvgqFY8iWQM0p4wLIqMXaXQ&usqp=CAU",
      title: "Cofee",
    },
    {
      img: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRrQFem8ZHJS440XEi7ACsbTR3rBTXtds2g2Q&usqp=CAU",
      title: "Flower",
    },
    {
      img: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTUkHkLF2-8Nj_lBHOOuFMa_b9XpUjj5v6w2Q&usqp=CAU",
      title: "Nature",
    },
    {
      img: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQv4qTlFZcK34lhjX4BwkO08RRNWjY1QyS7og&usqp=CAU",
      title: "Meals",
    },
    {
      img: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSyrCYAblBhipIEgZaxsNNGqCEu2LkFAQ36Rw&usqp=CAU",
      title: "Animals",
    }, {
      img: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSJMSrrmX3sr68FNHKMRkr5yOKn2c1SlSIdhQ&usqp=CAU",
      title: "Books",
    }, {
      img: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQTvZSh0y1qzROJxav5URZA8Y_aHYlkgjgzyQ&usqp=CAU",
      title: "River",
    }, {
      img: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQOckYSyMRNTx49FQ7HjH4Wvg1QB7dbpgHyXw&usqp=CAU",
      title: "Mother",
    }, {
      img: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTu8jSdMzTGjaCpZUBDPChAJPpvZhwIgd0FOA&usqp=CAU",
      title: "Deer",
    }, {
      img: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRq_UZ2gMm6naA4V72hVaVFo5v8duuPB6SkKA&usqp=CAU",
      title: "Icecream",
    }, {
      img: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTCmvuFJ7tXtqxgG49vDUeVtxp-8KCu8HVlng&usqp=CAU",
      title: "Honey",
    }, {
      img: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSyrCYAblBhipIEgZaxsNNGqCEu2LkFAQ36Rw&usqp=CAU",
      title: "Animals",
    }, {
      img: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSyrCYAblBhipIEgZaxsNNGqCEu2LkFAQ36Rw&usqp=CAU",
      title: "Animals",
    }, {
      img: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSyrCYAblBhipIEgZaxsNNGqCEu2LkFAQ36Rw&usqp=CAU",
      title: "Animals",
    }, {
      img: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSyrCYAblBhipIEgZaxsNNGqCEu2LkFAQ36Rw&usqp=CAU",
      title: "Animals",
    }, {
      img: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSyrCYAblBhipIEgZaxsNNGqCEu2LkFAQ36Rw&usqp=CAU",
      title: "Animals",
    }, {
      img: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSyrCYAblBhipIEgZaxsNNGqCEu2LkFAQ36Rw&usqp=CAU",
      title: "Animals",
    }, {
      img: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSyrCYAblBhipIEgZaxsNNGqCEu2LkFAQ36Rw&usqp=CAU",
      title: "Animals",
    }, {
      img: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSyrCYAblBhipIEgZaxsNNGqCEu2LkFAQ36Rw&usqp=CAU",
      title: "Animals",
    },
  ];
  return (
    <Stack spacing={4}>
      <ImageList sx={{ width: 500, height: 450 }} cols={3} rowHeight={164}>
        {imageData.map((item) => (
          <ImageListItem key={item.img}>
            <img
              src={`${item.img}?w=164&fit=crop&auto=format&dpr=2`}
              alt={item.title}
              loading="lazy"
            />
          </ImageListItem>
        ))}
      </ImageList>
      <ImageList variant='woven' sx={{ width: 500, height: 450 }} cols={3} gap={8}>
        {imageData2.map((item) => (
          <ImageListItem key={item.img}>
            <img
              src={`${item.img}?w=164&fit=crop&auto=format&dpr=2`}
              alt={item.title}
              loading="lazy"
            />
          </ImageListItem>
        ))}
      </ImageList>
      <Box sx={{ width: 500, height: 450, overflowY:'scroll'}}>
      <ImageList variant="masonry" cols={3} gap={8}>
        {imageData3.map((item) => (
          <ImageListItem key={item.img}>
            <img
              src={`${item.img}?w=248&fit=crop&auto=format&dpr=2`}
              alt={item.title}
              loading="lazy"
            />
            <ImageListItemBar title={item.title}/>
          </ImageListItem>
        ))}
      </ImageList>
      </Box>
    </Stack>
  );
}

export default l;
