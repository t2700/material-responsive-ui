import React from 'react'
import {Stack,Avatar,AvatarGroup} from '@mui/material'
function MuiAvatar() {
  return (
   <Stack spacing={4}>
       <Stack direction='row' spacing={1}>
           <Avatar sx={{bgcolor:'primary.light'}}>BW</Avatar>
           <Avatar sx={{bgcolor:'success.light'}}>CK</Avatar>
       </Stack>
       <Stack direction='row' spacing={1}>
           <AvatarGroup max={3}>
           <Avatar sx={{bgcolor:'primary.light'}}>BW</Avatar>
           <Avatar sx={{bgcolor:'success.light'}}>CK</Avatar>
           <Avatar src='https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTnP19X1E0Ym42cvyPOL7BHzp7pSZ0R0vNimw&usqp=CAU' alt='yashu'/>
           <Avatar src='https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSPOEK3osmoZYCn8G4qLpn9EPaqRnQsXkcbcA&usqp=CAU' alt='anu'/>
           </AvatarGroup>
       </Stack>
       <Stack direction='row' spacing={1}>
           <Avatar variant='square' sx={{bgcolor:'primary.light',width:48,height:48}}>BW</Avatar>
           <Avatar variant='rounded' sx={{bgcolor:'success.light',width:48,height:48}}>CK</Avatar>
       </Stack>
   </Stack>
  )
}

export default MuiAvatar