import React,{useState} from 'react';
import{Stack,Autocomplete,TextField} from '@mui/material';

// let skill = {
//   id:"number",
//   label:"string"
// }
const skills =['html','css','javascript','Typescript','react']
const skillsOptions = skills.map((skill,index)=>({
id:index+1,
label:skill,
}))
function MuiAutoComplete() {
const[value,setvalue]= useState(null)
const[skill,setSkill]= useState(null)
console.log(skill)
  return (
   <Stack spacing={2} width='250px'>
     <Autocomplete options={skills}
      renderInput={(params)=><TextField {...params} label="Skills"/>} 
      value={value}
      onChange={(e,newval)=>setvalue(newval)}
      freeSolo
      />
      <Autocomplete options={skillsOptions}
      renderInput={(params)=><TextField {...params} label="Skills"/>} 
      value={skill}
      onChange={(e,newval)=>setSkill(newval)}
      />
   </Stack>
  )
}

export default MuiAutoComplete