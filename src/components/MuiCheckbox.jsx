import React from "react";
import {
  Box,
  FormControlLabel,
  Checkbox,
  FormControl,
  FormLabel,
  FormGroup,
  FormHelperText
} from "@mui/material";
import { useState } from "react";
import BookmarkBorderIcon from "@mui/icons-material/BookmarkBorder";
import BookmarkIcon from "@mui/icons-material/Bookmark";

function MuiCheckbox() {
  const [acceptInC, setAcceptInC] = useState(false);
  const [skills, setSkills] = useState([]);
  console.log(skills)

  console.log(acceptInC);
  const handleChange = (e) => {
    setAcceptInC(e.target.checked);
  };
  const handleSkillChange = (e) => {
      debugger
    const index =skills.indexOf(e.target.value)
    if(index === -1){
        setSkills([...skills,e.target.value])
    }else{
        setSkills(skills.filter((skill)=> skill !== e.target.value))
    }

  };
  return (
    <Box>
      <Box>
        <FormControlLabel
          label="i accept terms and conditions"

          control={<Checkbox size="small" color="secondary" checked={acceptInC} onChange={handleChange} />}
        />
      </Box>
      <Box>
        <Checkbox
          icon={<BookmarkBorderIcon />}
          checkedIcon={<BookmarkIcon />}
          checked={acceptInC}
          onChange={handleChange}
        />
      </Box>
      <Box>
        <FormControl>
          <FormLabel error>Skills</FormLabel>
          <FormGroup>
            <FormControlLabel
              label="HTML"
              control={
                <Checkbox
                  value="html"
                  checked={skills.includes('html')}
                  onChange={handleSkillChange}
                />
              }
            />
            <FormControlLabel
              label="CSS"
              control={
                <Checkbox
                  value="css"
                  checked={skills.includes('css')}
                  onChange={handleSkillChange}
                />
              }
            />
            <FormControlLabel
              label="JavaScript"
              control={
                <Checkbox
                  value="javascript"
                  checked={skills.includes('javascript')}
                  onChange={handleSkillChange}
                />
              }
            />
          </FormGroup>
          <FormHelperText>Invalid selection</FormHelperText>
        </FormControl>
      </Box>
    </Box>
  );
}

export default MuiCheckbox;
