import React from 'react'
import {Box,TextField,MenuItem} from '@mui/material';
import {useState} from 'react'

function MuiSelect() {
   const [country,setCountry]=useState([]) 
   console.log(country);
   const handleChange = (event) => {
       setCountry(event.target.value)
   }
  return (
    <div>
        <Box width='250px'>
            <TextField label='select country' select 
            value={country} 
            onChange={handleChange}
            fullWidth
            SelectProps={{
                multiple:true
            }}
            size='small'
            color='secondary'
            helperText='Please select your country'
            >
                <MenuItem value='IN'>India</MenuItem>
                <MenuItem value='US'>USA</MenuItem>
                <MenuItem value='AU'>Austraia</MenuItem>
            </TextField>
        </Box>
    </div>
  )
}

export default MuiSelect