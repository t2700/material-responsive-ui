import React,{useState} from 'react';
import {Stack,TextField} from '@mui/material';
import {DatePicker,TimePicker,DateTimePicker} from '@mui/lab'

function MuiPicker() {
    const[selectedDate,setSelectedDate]= useState(null)
    const[selectedTime,setSelectedTime]= useState(null)
    const[selectedDateTime,setSelectedDateTime]= useState(null)
    console.log(selectedDate)
  return (
    
    <Stack spacing={4} sx={{width:'250px'}}>
        <DatePicker 
        label='Date Picker' 
        renderInput={(params)=><TextField {...params} />}
        value={selectedDate}
        onChange={(newvalue)=>{setSelectedDate(newvalue)}}
        />  
        <TimePicker 
        label='Time Picker' 
        renderInput={(params)=><TextField {...params} />}
        value={selectedTime}
        onChange={(newvalue)=>{setSelectedTime(newvalue)}}
        />
         <DateTimePicker 
        label='Date Time Picker' 
        renderInput={(params)=><TextField {...params} />}
        value={selectedDateTime}
        onChange={(newvalue)=>{setSelectedDateTime(newvalue)}}
        />
    </Stack>
   
  )
}

export default MuiPicker