import './App.css';
import { LocalizationProvider } from '@mui/lab';
import { createTheme,colors,ThemeProvider } from '@mui/material';
import AdapterDateFns from '@mui/lab/AdapterDateFns';
import MuiAccordion from './components/MuiAccordion';
import MuiAlert from './components/MuiAlert';
import MuiAutoComplete from './components/MuiAutoComplete';
import MuiAvatar from './components/MuiAvatar';
import MuiBadge from './components/MuiBadge';
import MuiBottomNavigation from './components/MuiBottomNavigation';
import MuiBreadcrumbs from './components/MuiBreadcrumbs';
import MuiCard from './components/MuiCard';
import MuiCheckbox from './components/MuiCheckbox';
import MuiChip from './components/MuiChip';
import MuiDialog from './components/MuiDialog';
import MuiDrawer from './components/MuiDrawer';
import MuiImageList from './components/MuiImageList';
import MuiLayout from './components/MuiLayout';
import MuiLink from './components/MuiLink';
import MuiList from './components/MuiList';
import MuiLoadingButton from './components/MuiLoadingButton';
import MuiNavbar from './components/MuiNavbar';
import MuiProgress from './components/MuiProgress';
import MuiRadioButton from './components/MuiRadioButton';
import MuiRating from './components/MuiRating';
import MuiSelect from './components/MuiSelect';
import MuiSkeleton from './components/MuiSkeleton';
import MuiSnackbar from './components/MuiSnackbar';
import MuiSpeedDial from './components/MuiSpeedDial';
import MuiSwitch from './components/MuiSwitch';
import MuiTable from './components/MuiTable';
// import MuiButton from './components/MuiButton';
import MuiTextField from './components/MuiTextField';
import MuiTooltip from './components/MuiTooltip';
import MuiPicker from './components/MuiPicker';
import MuiDateRangePicker from './components/MuiDateRangePicker';
import Muitabs from './components/Muitabs';
import MuiTimeline from './components/MuiTimeline';
import MuiMasonry from './components/MuiMasonry';
import MuiResponsiveness from './components/MuiResponsiveness';
// import MuiTypography from './components/MuiTypography';

const theme = createTheme({
  status:{
    danger:'#e53e3e'
  },
  palette:{
    secondary:{
      main: colors.orange[500]
    },
    neutral:{
      main:colors.grey[500],
      darker:colors.grey[700],
    }
  }
});
function App() {
  return (
    <ThemeProvider theme={theme}>
    <LocalizationProvider dateAdapter={AdapterDateFns}>
    <div className="App">
     {/* <MuiTypography/> */}
     {/* <MuiButton/> */}
     {/* <MuiTextField/> */}
     {/* <MuiSelect/> */}
     {/* <MuiRadioButton/> */}
     {/* <MuiCheckbox/> */}
     {/* <MuiSwitch/> */}
     {/* <MuiRating/> */}
     {/* <MuiAutoComplete/> */}
     {/* <MuiLayout/> */}
     {/* <MuiCard/> */}
     {/* <MuiAccordion/> */}
     {/* <MuiImageList/> */}
     {/* <MuiNavbar/>
     <MuiLink/>
     <MuiBreadcrumbs/>
     <MuiDrawer/>
     <MuiSpeedDial/>
     <MuiBottomNavigation/> */}
     {/* <MuiAvatar/> */}
     {/* <MuiBadge/>
     <MuiList/>
     <MuiChip/> */}
     {/* <MuiTooltip/> */}
     {/* <MuiTable/> */}
     {/* <MuiAlert/> */}
     {/* <MuiSnackbar/> */}
     {/* <MuiDialog/> */}
     {/* <MuiProgress/> */}
     {/* <MuiSkeleton/> */}
     {/* <MuiLoadingButton/> */}
     {/* <MuiPicker/> */}
     {/* <MuiDateRangePicker/> */}
     {/* <Muitabs/> */}
     {/* <MuiTimeline/> */}
     {/* <MuiMasonry/> */}
     <MuiResponsiveness/>
    </div>
    </LocalizationProvider>
    </ThemeProvider>
  );
}

export default App;
